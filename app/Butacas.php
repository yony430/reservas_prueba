<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Butacas extends Model {

    protected $primaryKey = 'idbutaca';
    protected $table = 'butacas';
    protected $fillable = ['id_reserva', 'fila', 'numero_butaca'];
    public $timestamps = false;

}
