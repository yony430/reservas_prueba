<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model {

    protected $table = "evento";
    protected $primaryKey = "idevento";
    protected $fillable = ['id_sala', 'nombre', 'descripcion', 'fecha', 'created_at'];

    public function sala() {
        return $this->belongsTo('App\Sala', 'id_sala');
    }

}
