<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserva extends Model {

    protected $table = "reserva";
    protected $primaryKey = "idreserva";
    protected $fillable = ['id_evento', 'id_usuario', 'created_at'];

    public function evento() {
        return $this->belongsTo('App\Evento', 'id_evento');
    }

    public function usuario() {
        return $this->belongsTo('App\User', 'id_usuario');
    }

    public function reserva_butacas() {
        return $this->hasMany('App\Butacas', 'id_reserva');
    }

}
