<?php

namespace App\Http\Controllers;

use App\Reserva;
use App\Evento;
use App\Butacas;
use Illuminate\Http\Request;
use DateTime;
use Illuminate\Support\Facades\Auth;

class ReservaController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $now = new DateTime();
        $eventos = Evento::where(
                        "fecha", ">", $now->format("Y-m-d H:m:s")
                )->get();
        return view('reservas.index', compact('eventos'));
    }

    public function misreservas() {

        $reservas = Reserva::where([
                    ["id_usuario", Auth::id()]
                ])->get();


        return view('reservas.misreservas', compact("reservas"));
    }

    public function modalreserva(Request $request) {

        $reservas = Reserva::where([
                    ["id_evento", $request->get("idevento")]
                ])->get();

        $butacas = array();
        foreach ($reservas as $key => $reserva) {
            $br = $reserva->reserva_butacas;

            foreach ($br as $key => $but) {
                $butacas[] = $but;
            }
        }
//        return $butacas;
        $evento = Evento::find($request->get("idevento"));
        return view("reservas.modalreserva", compact("evento", "reservas", "butacas"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {
        //

        $archivo = fopen("log.txt", "a");

        $user = Auth::user();

        $idUser = Auth::id();
        $array_reserva = $request->get("reserva");
        $idevento = $request->get("idevento");

        $evento = Evento::find($idevento);

        $reserva = Reserva::create([
                    "id_evento" => $idevento,
                    "id_usuario" => $idUser,
        ]);

        fwrite($archivo, date("d m Y H:m:s") . " " . "NUEVA RESERVA Usuario: " . $user->name . " Evento: " . $evento->nombre . "\n");


        for ($i = 0; $i < count($array_reserva); $i++) {
            Butacas::create([
                "id_reserva" => $reserva->idreserva,
                "fila" => $array_reserva[$i]["f"],
                "numero_butaca" => $array_reserva[$i]["b"]
            ]);

            fwrite($archivo, "       -> " . "Fila: " . $array_reserva[$i]["f"] . " Butaca: " . $array_reserva[$i]["b"] . "\n");
        }

        fwrite($archivo, "\n");
        fclose($archivo);


        return $reserva;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reserva  $reserva
     * @return \Illuminate\Http\Response
     */
    public function show(Reserva $reserva) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reserva  $reserva
     * @return \Illuminate\Http\Response
     */
    public function edit(Reserva $reserva) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reserva  $reserva
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reserva $reserva) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reserva  $reserva
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reserva $reserva) {
        //
    }

}
