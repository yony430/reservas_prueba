<?php

namespace App\Http\Controllers;

use App\Sala;
use Illuminate\Http\Request;

class SalaController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $salas = Sala::all();
        return view('salas.index', compact('salas'));
    }

    public function modalnuevasala(Request $request) {
        return view("salas.modalnuevasala");
    }

    public function modaleditsala(Request $request) {
        $sala = Sala::find($request->get("idsala"));

        #Se consulta si la sala existe en un evento  (no se podria eliminar)

        $existeEnEvento = Sala::whereIn("idsala", function($query) {
                    $query->select('id_sala')
                    ->from('evento');
                })
                ->where("idsala", $request->get("idsala"))
                ->get();

        return view("salas.modaleditsala", compact("sala", "existeEnEvento"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {
        return Sala::create([
                    "nombre" => $request->get("nombre"),
                    "filas" => $request->get("filas"),
                    "butacas_fila" => $request->get("butacas_fila")
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sala  $sala
     * @return \Illuminate\Http\Response
     */
    public function show(Sala $sala) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sala  $sala
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request) {
        //
        $sala = Sala::find($request->get("idsala"));
        $sala->nombre = $request->get("nombre");
        $sala->filas = $request->get("filas");
        $sala->butacas_fila = $request->get("butacas_fila");
        $sala->save();

        return $sala;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sala  $sala
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sala $sala) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sala  $sala
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sala $sala) {
        //
    }
    
    public function delete($idSala) {
        $sala = Sala::find($idSala);
        $sala->delete();
    }

}
