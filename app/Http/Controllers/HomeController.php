<?php

namespace App\Http\Controllers;

use App\Evento;
use Illuminate\Http\Request;
use DateTime;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        
         $now = new DateTime();
        
        $eventos = Evento::where([
                    ["fecha", '>', $now->format("Y-m-d H:m:s")]
                ])->limit(10)->get();
        return view('home', compact('eventos'));
    }

}
