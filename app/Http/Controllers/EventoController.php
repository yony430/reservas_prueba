<?php

namespace App\Http\Controllers;

use App\Evento;
use App\Sala;
use Illuminate\Http\Request;

class EventoController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $eventos = Evento::all();
        return view('eventos.index', compact('eventos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {
        return Evento::create([
                    "nombre" => $request->get("nombre"),
                    "descripcion" => $request->get("descripcion"),
                    "id_sala" => $request->get("id_sala"),
                    "fecha" => $request->get("fecha")
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Evento  $evento
     * @return \Illuminate\Http\Response
     */
    public function show(Evento $evento) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Evento  $evento
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request) {
        $evento = Evento::find($request->get("idevento"));
        $evento->nombre = $request->get("nombre");
        $evento->id_sala = $request->get("id_sala");
        $evento->descripcion = $request->get("descripcion");
        $evento->fecha = $request->get("fecha");
        $evento->save();

        return $evento;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Evento  $evento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Evento $evento) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Evento  $evento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Evento $evento) {
        //
    }

    public function modalnuevoevento() {
        $salas = Sala::all();
        return view("eventos.modalnuevoevento", compact('salas'));
    }

    public function modaleditevento(Request $request) {
        $evento = Evento::find($request->get("idevento"));
        $salas = Sala::all();

        return view("eventos.modaleditevento", compact('salas', 'evento'));
    }

}
