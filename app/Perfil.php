<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model {

    protected $primaryKey = 'idperfil';
    protected $table = 'perfil';
    protected $fillable = ['idperfil', 'nombre'];
    public $timestamps = false;

}
