<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sala extends Model {

    protected $primaryKey = 'idsala';
    protected $table = 'sala';
    protected $fillable = ['nombre', 'filas', 'butacas_fila', 'created_at'];
    public $timestamps = false;

}
