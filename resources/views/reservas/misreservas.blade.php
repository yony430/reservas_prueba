@extends('layouts.dashboard')

@section('content')

<!-- Creamos la tabla para indicar que salas existen -->
<div class="container">
    <hr>
    <div class="row">
        <div class="col">
            <h1>Mis Reservas</h1>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col">

            <table id="tblSalas" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Evento</th>
                        <th>Descripci&oacute;n</th>
                        <th>Sala</th>
                        <th>Fila/Butaca</th>
                        <th>Fecha</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($reservas as $key=>$reserva)
                    <tr>
                        <td>{{$reserva->idreserva}}</td>
                        <td>{{$reserva->evento->nombre}}</td>
                        <td>{{$reserva->evento->descripcion}}</td>
                        <td>{{$reserva->evento->sala->nombre}}</td>
                        <td>
                            @php 
                            $arrButacas = $reserva->reserva_butacas;
                            @endphp
                            <ul>
                                @foreach ($arrButacas as $key=>$butacar)
                                <li>{{$butacar->fila}}-{{$butacar->numero_butaca}}</li>
                                @endforeach
                            </ul>
                        </td>
                        <td>{{$reserva->evento->fecha}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<script src="{{ asset('js/reservas.js') }}" defer></script>

@endsection
