@extends('layouts.dashboard')

@section('content')

<!-- Creamos la tabla para indicar que salas existen -->
<div class="container">
    <hr>
    <div class="row">
        <div class="col">
            <h1>Nueva Reserva</h1>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col">

            <table id="tblSalas" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Evento</th>
                        <th>Descripci&oacute;n</th>
                        <th>Sala</th>
                        <th>Fecha</th>
                        <th>Reservar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($eventos as $key=>$evento)
                    <tr>
                        <td>{{$evento->idevento}}</td>
                        <td>{{$evento->nombre}}</td>
                        <td>{{$evento->descripcion}}</td>
                        <td>{{$evento->sala->nombre}}</td>
                        <td>{{$evento->fecha}}</td>
                        <td>
                            <button class="btn" idevento="{{$evento->idevento}}" onclick="reservar($(this));">
                                <i class="fas fa-ticket-alt"></i>
                            </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<script src="{{ asset('js/reservas.js') }}" defer></script>

@endsection
