<style>
    @media screen and (min-width: 768px) {
        .modal-dialog {
            width: 80%; 
            max-width: 80% !important;
        }
        .modal-sm {
            width: 350px; 
            max-width: 350px !important;
        }
    }
    @media screen and (min-width: 992px) {
        .modal-lg {
            width: 1250px;
            max-width: 1250px !important;
        }
    }

    :-ms-input-placeholder{ padding-top:2px;}

</style>


<div class="modal fade" id="resevaModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="crear_reserva" role="form" method="POST" action="{{ url('/reserva') }}" enctype="multipart/form-data">
                <input type="hidden" id="idevento" name="idevento" value="{{$evento->idevento }}">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nueva reserva ({{$evento->nombre }} [{{$evento->fecha}}] )</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h3>Sala :  {{ucfirst($evento->sala->nombre)}}</h3>



                    <div class="row">
                        <div class="col-md-8">
                            <div style=" overflow-x: auto;white-space: nowrap;">
                                @for ($f = 1; $f <= $evento->sala->filas; $f++)
                                <label title="Fila {{$f}}" style="padding-top: 10px;width: 15px;">{{$f}}</label>
                                @for ($b = 1; $b <= $evento->sala->butacas_fila; $b++)

                                @php 
                                $disponible = true;

                                for($ex = 0;$ex < count($butacas);$ex++){
                                if($butacas[$ex]->numero_butaca == $b && $butacas[$ex]->fila == $f){
                                $disponible = false;
                                }
                                }
                                @endphp


                                @if($disponible)
                                <img class="r-off" src="/img/logo_live_appear_offline.png" height="30" width="30" f="{{$f }}" title="Butaca {{$b}}" b="{{$b}}" style="margin-left: 10px;margin-top: 5px;cursor: pointer;" onclick="reservaAct($(this));">
                                @else
                                <img  src="/img/logo_live_busy.png" height="30" width="30" f="{{$f }}" title="Butaca {{$b}} (Ocupada)" b="{{$b}}" style="margin-left: 10px;margin-top: 5px;cursor: pointer;">
                                @endif
                                @endfor
                                <br>
                                @endfor
                            </div>
                        </div>
                        <div class="col-md-4">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Fila</th>
                                        <th scope="col">Butaca</th>
                                    </tr>
                                </thead>
                                <tbody class="tbl-resumen-r">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary btn-ok-r">Aceptar</button>
                </div>

            </form>

        </div>
    </div>
</div>