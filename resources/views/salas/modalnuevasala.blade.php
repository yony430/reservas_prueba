<form id="crear_sala" role="form" method="POST" action="{{ url('/sala') }}" enctype="multipart/form-data">
    <div class="modal fade" id="salaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nueva sala</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-12 b-r">
                            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                <label id="106">Nombre *</label> 
                                <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre" value="" required>
                                @if ($errors->has('nombre'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('nombre') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-6 b-r">
                            <div class="form-group {{ $errors->has('filas') ? ' has-error' : '' }}">
                                <label id="106">Filas *</label> 
                                <input type="number" name="filas" class="form-control" id="filas" placeholder="Filas" value="" required>
                                @if ($errors->has('filas'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('filas') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-6 b-r">
                            <div class="form-group {{ $errors->has('butacas_fila') ? ' has-error' : '' }}">
                                <label id="106">Butacas en fila *</label> 
                                <input type="number" name="butacas_fila" class="form-control" id="butacas_fila" placeholder="Butacas en fila" value="" required>
                                @if ($errors->has('butacas_fila'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('butacas_fila') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Aceptar</button>
                </div>
            </div>
        </div>
    </div>
</form>