@extends('layouts.dashboard')

@section('content')

<!-- Creamos la tabla para indicar que salas existen -->
<div class="container">
    <hr>
    <div class="row">
        <div class="col">
            <h1>Salas</h1>
        </div>
        <div class="col text-right">
            <button type="button" class="btn btn-success btn-add">Nueva sala</button>  
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col">

            <table id="tblSalas" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Filas</th>
                        <th>Butacas por filas</th>
                        <th>Editar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($salas as $key=>$sala)
                    <tr>
                        <td>{{$sala->idsala}}</td>
                        <td>{{$sala->nombre}}</td>
                        <td>{{$sala->filas}}</td>
                        <td>{{$sala->butacas_fila}}</td>
                        <td>
                            <button class="btn" idsala="{{$sala->idsala}}" onclick="edit($(this));">
                                <i class="fas fa-edit"></i>
                            </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="content-modal"></div>

<script src="{{ asset('js/salas.js') }}" defer></script>

@endsection
