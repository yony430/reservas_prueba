<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }}</title>

        <link href="/css/bootstrap.min.css" rel="stylesheet">
        <link href="/css/datatables.css" rel="stylesheet">
        <link href="/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

        <script src="{{ asset('js/jquery-3.1.1.min.js') }}" defer></script>
        <script src="{{ asset('js/bootstrap.min.js') }}" defer></script>
        <script src="{{ asset('js/datatables.min.js') }}" defer></script>
        <script src="{{ asset('js/jquery.form.js') }}" defer></script>
        <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}" defer></script>
    </head>
    <body>
        <header>
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="/home">{{ config('app.name', 'Laravel') }}</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        @if(Auth::user()->id_perfil === 1)
                        <li class="nav-item">
                            <a class="nav-link" href="/salas">Salas</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/eventos">Eventos</a>
                        </li>
                        @endif
                        <li class="nav-item">
                            <a class="nav-link" href="/reservas">Nueva Reserva</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/misreservas">Mis Reservas</a>
                        </li>

                    </ul>
                    <form class="form-inline my-2 my-lg-0">

                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ Auth::user()->name }}
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
    document.getElementById('logout-form').submit();">
                                        Cerrar sesión
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </form>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </nav>
        </header>

        <main>
            @yield('content')    
        </main>

        <hr>

        <footer class="container">
            <p class="float-right"><a href="#">Ir arriba</a></p>
            <p>© 2018 Yony Arroyave. · <a href="#">Prueba de desarrolllo</a></p>
        </footer>

        <div class="content-modal"></div>

    </body>
</html>