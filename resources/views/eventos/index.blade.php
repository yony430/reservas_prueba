@extends('layouts.dashboard')

@section('content')

<!-- Creamos la tabla para indicar que salas existen -->
<div class="container">
    <hr>
    <div class="row">
        <div class="col">
            <h1>Eventos</h1>
        </div>
        <div class="col text-right">
            <button type="button" class="btn btn-success btn-add">Nuevo evento</button>  
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col">

            <table id="tblSalas" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Descripci&oacute;n</th>
                        <th>Sala</th>
                        <th>Fecha</th>
                        <th>Editar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($eventos as $key=>$evento)
                    <tr>
                        <td>{{$evento->idevento}}</td>
                        <td>{{$evento->nombre}}</td>
                        <td>{{$evento->descripcion}}</td>
                        <td>{{$evento->sala->nombre}}</td>
                        <td>{{$evento->fecha}}</td>
                        <td>
                            <button class="btn" idevento="{{$evento->idevento}}" onclick="edit($(this));">
                                <i class="fas fa-edit"></i>
                            </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="content-modal"></div>

<script src="{{ asset('js/eventos.js') }}" defer></script>

@endsection
