<form id="editar_evento" role="form" method="POST" action="{{ url('/eventoedit') }}" enctype="multipart/form-data">
    <input type="hidden" id="idevento" name="idevento" value="{{$evento->idevento}}">
    <div class="modal fade" id="eventoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nuevo evento</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-12 b-r">
                            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                <label id="106">Nombre *</label> 
                                <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre" value="{{$evento->nombre}}" required>
                                @if ($errors->has('nombre'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('nombre') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-12 b-r">
                            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                <label id="106">Descripci&oacute;n *</label> 
                                <textarea name="descripcion" class="form-control" id="descripcion" placeholder="Descripción" value="" required style="resize: none;">{{$evento->descripcion}}</textarea>
                                @if ($errors->has('descripcion'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('nombre') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-6 b-r">
                            <div class="form-group {{ $errors->has('id_sala') ? ' has-error' : '' }}">
                                <label id="106">Sala *</label> 
                                <select type="number" name="id_sala" class="form-control" id="id_sala" placeholder="Sala" required>
                                    <option value=""></option>
                                    @foreach ($salas as $key=>$sala)
                                    <option {{ ($sala->idsala === $evento->id_sala) ? 'selected="true"' : '' }} value="{{$sala->idsala}}">{{$sala->nombre}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('id_sala'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('id_sala') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-6 b-r">
                            <div class="form-group {{ $errors->has('fecha') ? ' has-error' : '' }}">
                                <label id="106">Fecha *</label> 
                                <input type="text" name="fecha" class="form-control" id="fecha" placeholder="Fecha" value="{{$evento->fecha}}" required readonly="" data-format="dd/MM/yyyy hh:mm:ss">
                                @if ($errors->has('fecha'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('fecha') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Aceptar</button>
                </div>
            </div>
        </div>
    </div>
</form>