@extends('layouts.dashboard')

@section('content')
<style>
    .carousel{
        margin-bottom: 4rem;
    }

    .carousel-item > img {
        /*position: absolute;*/
        top: 0;
        left: 0;
        min-width: 100%;
        height: 16rem;
    }

</style>

<div id="carouselEventos" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        @foreach ($eventos as $key=>$evento)
        <li data-target="#carouselEventos" data-slide-to="{{$key}}" class="active"></li>
        @endforeach
    </ol>
    <div class="carousel-inner">

        @foreach ($eventos as $key=>$evento)
        <div class="carousel-item {{($key==0) ? 'active' : ''}}">
            <img class="first-slide" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="First slide">
            <div class="container">
                <div class="carousel-caption text-left">
                    <h1>{{$evento->nombre}}</h1>
                    <p>{{$evento->descripcion}}</p>
                    <p><a class="btn btn-lg btn-primary" idevento="{{$evento->idevento}}" role="button" onclick="reservar($(this));">Reservar</a></p>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <a class="carousel-control-prev" href="#carouselEventos" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselEventos" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<div class="container marketing">

    <!-- Three columns of text below the carousel -->
    <div class="row">
        @if(Auth::user()->id_perfil === 1)
        <div class="col-lg-4" style="text-align: center;">
            <img class="rounded-circle" src="/img/salas.jpg" alt="Generic placeholder image" width="140" height="140">
            <h2>Salas</h2>
            <p>Crea salas, y administra el nómero de filas y butacas por fila.</p>
            <p><a class="btn btn-secondary" href="/salas" role="button">Ir »</a></p>
        </div>
        <div class="col-lg-4" style="text-align: center;">
            <img class="rounded-circle" src="/img/eventos.jpg" alt="Generic placeholder image" width="140" height="140">
            <h2>Eventos</h2>
            <p>Genera tus propios eventos y asigna una sala al mismo.</p>
            <p><a class="btn btn-secondary" href="/eventos" role="button">Ir »</a></p>
        </div>
        @endif

        <div class="col-lg-4" style="text-align: center;">
            <img class="rounded-circle" src="/img/tickets.jpg" alt="Generic placeholder image" width="140" height="140">
            <h2>Reservas</h2>
            <p>Genera una nueva reserva para tu evento favorito, no te quedes por fuera.</p>
            <p ><a class="btn btn-secondary" href="/reservas" role="button">Ir »</a></p>
        </div>
    </div>

</div>

<script src="{{ asset('js/reservas.js') }}" defer></script>

@endsection
