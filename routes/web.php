<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

#Rutas para salas
Route::get('/salas', 'SalaController@index')->name('salas');
Route::post('/modalsala', 'SalaController@modalnuevasala');
Route::post('/modalsalaedit', 'SalaController@modaleditsala');
Route::post('/sala', 'SalaController@create')->name('guardarsala');
Route::post('/salaedit', 'SalaController@edit')->name('editarsala');
Route::get('/saladelete/{id}', 'SalaController@delete')->name('eliminarsala');

#Rutas para eventos
Route::get('/eventos', 'EventoController@index')->name('eventos');
Route::post('/modalevento', 'EventoController@modalnuevoevento');
Route::post('/evento', 'EventoController@create')->name('guardarevento');
Route::post('/modaleventoedit', 'EventoController@modaleditevento');
Route::post('/eventoedit', 'EventoController@edit')->name('editarevento');

#Rutas para reservas
Route::get('/reservas', 'ReservaController@index')->name('reservas');
Route::get('/misreservas', 'ReservaController@misreservas')->name('misreservas');
Route::post('/modalreserva', 'ReservaController@modalreserva');
Route::post('/reserva', 'ReservaController@create')->name('guardarreserva');