$(document).ready(function () {
    $('#tblSalas').DataTable({
        "bJQueryUI": false,
        "bAutoWidth": false,
        "sPaginationType": "full_numbers",
        "iDisplayLength": 100,
        "iDisplayStart": 0,
        "order": [[0, "desc"]],
        "sDom": '<"datatable-header"fpl>t<"datatable-footer"ip>',
        "oLanguage": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
});

function reservar($this) {
    var idevento = $this.attr("idevento");

    $.ajax({
        url: '/modalreserva',
        data: {
            idevento: idevento
        },
        error: function () {

        },
        success: function (data) {
            $(".content-modal").empty().html(data);
            $("#resevaModal").modal();
            $(".btn-ok-r").unbind().click(function () {
                crearreserva();
            });
        },
        method: 'post',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

}

function reservaAct($this) {

    if ($this.hasClass("r-off")) {
        $this.attr({
            src: "/img/logo_live_online.png"
        }).removeClass("r-off").addClass("r-on");
    } else {
        $this.attr({
            src: "/img/logo_live_appear_offline.png"
        }).removeClass("r-on").addClass("r-off");
    }
    setResumen();
}

function setResumen() {
    $(".tbl-resumen-r").empty();
    $(".r-on").each(function () {
        var _fila = $(this).attr('f');
        var _butaca = $(this).attr('b');
        $(".tbl-resumen-r").append('<tr>\n\
                            <th scope="row">' + _fila + '</th>\n\
                            <td>' + _butaca + '</td>\n\
                        </tr>');
    });
}

function crearreserva() {
    var array_reserva = new Array();
    $(".r-on").each(function () {
        array_reserva.push({
            f: $(this).attr('f'),
            b: $(this).attr('b')
        });
    });

    if (array_reserva.length > 0) {
        var _url = $("#crear_reserva").attr('action');
        $.ajax({
            url: _url,
            data: {
                reserva: array_reserva,
                idevento: $(".content-modal").find("#idevento").val()
            },
            error: function () {

            },
            success: function (data) {
                if (data.idreserva) {
                    alert('Reserva creada con éxito!');
                    window.location.reload();
                }
            },
            method: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });



    } else {
        alert("No se han seleccionado butacas");
    }

}