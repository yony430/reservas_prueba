$(document).ready(function () {
    $('#tblSalas').DataTable({
        "bJQueryUI": false,
        "bAutoWidth": false,
        "sPaginationType": "full_numbers",
        "iDisplayLength": 100,
        "iDisplayStart": 0,
        "order": [[0, "desc"]],
        "sDom": '<"datatable-header"fpl>t<"datatable-footer"ip>',
        "oLanguage": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    $('.btn-add').click(function () {
        getModalNuevoEvento();
    });

});

function getModalNuevoEvento() {

    $.ajax({
        url: '/modalevento',
        data: {},
        error: function () {

        },
        success: function (data) {
            $(".content-modal").empty().html(data);
            $("#eventoModal").modal();
            $(".content-modal").find("#fecha").datetimepicker();
            $(".content-modal").find('#crear_evento').ajaxForm({
                dataType: 'json',
                success: function (data) {
                    if (data.idevento) {
                        $("#eventoModal").modal('hide');
                        window.location.reload();
                    }

                }, headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        },
        method: 'post',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
}

function edit($this) {
    $.ajax({
        url: '/modaleventoedit',
        data: {
            idevento: $this.attr("idevento")
        },
        error: function () {},
        success: function (data) {
            $(".content-modal").empty().html(data);
            $("#eventoModal").modal();
            $(".content-modal").find("#fecha").datetimepicker();
            $(".content-modal").find('#editar_evento').ajaxForm({
                dataType: 'json',
                success: function (data) {
                    if (data.idevento) {
                        $("#eventoModal").modal('hide');
                        window.location.reload();
                    }

                }, headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(".btn-delete").click(function () {
                deletesala();
            });
        },
        method: 'post',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
}