$(document).ready(function () {
    $('#tblSalas').DataTable({
        "bJQueryUI": false,
        "bAutoWidth": false,
        "sPaginationType": "full_numbers",
        "iDisplayLength": 100,
        "iDisplayStart": 0,
        "order": [[0, "desc"]],
        "sDom": '<"datatable-header"fpl>t<"datatable-footer"ip>',
        "oLanguage": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    $('.btn-add').click(function () {
        getModalNuevaSala();
    });

});

function getModalNuevaSala() {

    $.ajax({
        url: '/modalsala',
        data: {},
        error: function () {

        },
        success: function (data) {
            $(".content-modal").empty().html(data);
            $("#salaModal").modal();
            $(".content-modal").find('#crear_sala').ajaxForm({
                dataType: 'json',
                success: function (data) {
                    if (data.idsala) {
                        $("#salaModal").modal('hide');
                        window.location.reload();
                    }

                }, headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        },
        method: 'post',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
}

function edit($this) {
    $.ajax({
        url: '/modalsalaedit',
        data: {
            idsala: $this.attr("idsala")
        },
        error: function () {},
        success: function (data) {
            $(".content-modal").empty().html(data);
            $("#salaModal").modal();
            $(".content-modal").find('#editar_sala').ajaxForm({
                dataType: 'json',
                success: function (data) {
                    if (data.idsala) {
                        $("#salaModal").modal('hide');
                        window.location.reload();
                    }

                }, headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(".btn-delete").click(function () {
                deletesala();
            });
        },
        method: 'post',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
}

function deletesala() {
    if (confirm("Esta seguro de querer eliminar la sala?")) {
        $.ajax({
            url: '/saladelete/' + $("#idsala").val(),
            data: {
            },
            error: function () {},
            success: function (data) {
                window.location.reload();
            },
            method: 'get',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }

}

